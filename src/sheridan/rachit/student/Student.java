/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan.rachit.student;

/**
 *
 * @author Rachit
 */
class Student {

    private String program;

    /**
     * Get the value of program
     *
     * @return the value of program
     */
    public String getProgram() {
        return program;
    }

    /**
     * Set the value of program
     *
     * @param program new value of program
     */
    public void setProgram(String program) {
        this.program = program;
    }

    private String name;
   private String id;

   public Student (String name, String id)
   {
      this.name = name;
      this.id = id;
   }

   public String getName ()
   {
      return name;
   }

   public void setName (String name)
   {
      this.name = name;
   }

   public String getId ()
   {
      return id;
   }

   public void setId (String id)
   {
      this.id = id;
   }

    
}
