/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sheridan.rachit.student;

import java.util.Scanner;

/**
 *
 * @author Rachit
 */
public class StudentList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Student[] students = new Student[2];

      Scanner input = new Scanner(System.in);

      for (int i = 0; i < students.length; i++) {

         System.out.println("Enter the student's name");
         String name = input.nextLine();

         System.out.println("Enter the student's id");
         String id = input.nextLine();

         Student student = new Student(name, id);

         students[i] = student;
      }

      System.out.println("Printing the students:");

      String format = "The student's name and the id is: %s\n";

      for (Student student : students) {

         System.out.printf(format, student.getName());
         System.out.println(student.getId());
      }
   }

    }
    

